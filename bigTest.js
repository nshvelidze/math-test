var Big = require('big.js');

console.time("Big.js");
var startingPrice = new Big("8.99"),
    discounted = startingPrice.times("0.75"),
    discountedBy = discounted.minus("2.00"),
    plusDelivery = discountedBy.plus("1.99"),
    plusVat = plusDelivery.times("1.18"),
    plusBankFee = plusVat.times('1.015');
console.timeEnd("Big.js");

console.time("Big.js many");
for(var i = 0; i <= 1000; i++) {
    startingPrice = new Big("8.99");
    discounted = startingPrice.times("0.75");
    discountedBy = discounted.minus("2.00");
    plusDelivery = discountedBy.plus("1.99");
    plusVat = plusDelivery.times("1.18");
    plusBankFee = plusVat.times('1.015');
}
console.timeEnd("Big.js many");

console.log("startingPrice", startingPrice.toFixed(2));
console.log("discounted *75%", discounted.toFixed(2));
console.log("discountedBy -2.00", discountedBy.toFixed(2));
console.log("plusDelivery +1.99", plusDelivery.toFixed(2));
console.log("plusVat +18%", plusVat.toFixed(2));
console.log("plusBankFee +1.5%", plusBankFee.toFixed(2));